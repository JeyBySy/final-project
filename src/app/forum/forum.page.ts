import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.page.html',
  styleUrls: ['./forum.page.scss'],
})
export class ForumPage implements OnInit {


  Post: any = [];
  isDown;
   
  username = localStorage.getItem('username')
  public profile = `/profile/${this.username}`;

  public id:string=""
  public uname:string="";
  constructor(private http: HttpClient) { 
     this.uname = localStorage.getItem('username')
     this.id = localStorage.getItem('id')
  }

  ngOnInit() {
    //  this.http.get('http://localhost:3000/splash/')
     this.http.get('https://api-splash.herokuapp.com/splash')
    .subscribe( data=>{
      // console.log(data)
      this.Post = data;
      this.isDown = false
    });
  }

}
