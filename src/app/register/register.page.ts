import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  public signIn_user={
    fname: "",
    lname:"",
    uname:"",
    pword:""
  }
  constructor(private router:Router, public loadingController: LoadingController, public toastController: ToastController) { }

  ngOnInit() {
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Sign-Up Successfuly.',      
      duration: 2000
    });
    toast.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2800
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
    this.router.navigate(['/login'])
    this.presentToast()
  }

  signUp(){
    localStorage.setItem('firstName', this.signIn_user.fname)
    localStorage.setItem('lastName', this.signIn_user.lname)
    localStorage.setItem('username', this.signIn_user.uname)
    localStorage.setItem('password', this.signIn_user.pword)
    this.presentLoading()
        
  }

}
