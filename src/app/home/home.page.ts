import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../service/data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  selectTabs = 'YourFeed';
  Post: any = [];
  isDown;
   
  username = localStorage.getItem('username')
  public profile = `/profile/${this.username}`;

    public uname:string="";
    

  constructor(private http: HttpClient, private dataService: DataService) {

    this.uname = localStorage.getItem('username')
    // console.log(this.uname)
  }
 

  ngOnInit() {
    // this.http.get('http://localhost:3000/splash')
     this.http.get('https://api-splash.herokuapp.com/splash')
    .subscribe( data=>{
      // console.log(data)
      this.Post = data;
      this.isDown = false
    });
    //  this.dataService.getData().subscribe(data =>{
    //    console.log(data)
    //  })
  }
  
  openPost(postID){
    console.log(postID)

  }

  //Existing Data set
    menu: any[] = [
    {
      "name": "Adobo",
      "country": 'Philippines',
      "emoji": '🍚',
      "image": 'https://assets.bonappetit.com/photos/60419b8c970fc321b2086e48/1:1/w_2560%2Cc_limit/GO-Live-Pineapple-Pork-3.jpg',
      
    },
    {
      "name": "Burger and Fries",
      "country": 'USA',
      "emoji": '🍔',
      "image": 'https://loveincorporated.blob.core.windows.net/contentimages/gallery/bc96df8d-4eb3-4249-86aa-9b11fce9c377-michigan-california-burgerz.jpg'
    },
    {
      "name": "Wonton Noodles",
      "country": 'Hong Kong',
      "emoji": '🍜',
      "image": 'https://christieathome.com/wp-content/uploads/2020/06/wonton-mein1-scaled.jpg'
    },
    {
      "name": "Beef Wellington",
      "country": 'UK',
      "emoji": '🍞', 
      "image": 'https://hips.hearstapps.com/delish/assets/18/11/1520886453-beef-wellington-delish.jpg'
    },
    {
      "name": "Sushi",
      "country": 'Japan',
      "emoji": '🍣',
      "image": 'https://images.japancentre.com/recipes/pics/18/main/makisushi.jpg?1557308201'
    }
  ];
}
