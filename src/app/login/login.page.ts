import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private router: Router, public toastController: ToastController,  public formBuilder: FormBuilder) { }

   public form={
    username:"",
    password:"",
  }


  ngOnInit() {

  }
async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,      
      duration: 2000
    });
    toast.present();
  }
  post(){
      // console.log(this.form)
      // this.router.navigate(['/usr/home'])

      if(localStorage.getItem('username') == this.form.username && localStorage.getItem('password') == this.form.password){
        this.router.navigate(['/usr/home'])
        localStorage.setItem("isLogin","true")
      }
      else{
        this.presentToast("Invalid Username or Password");
      }
      if(this.form.username === "" || this.form.password === ""){
         this.presentToast("Cannot accept empty field");
      }
      
  }
}
