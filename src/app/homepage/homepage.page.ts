import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../service/data.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.page.html',
  styleUrls: ['./homepage.page.scss'],
})
export class HomepagePage implements OnInit {

  Post: any = [];
  username = localStorage.getItem('username')
  public profile = `${this.username}`;
 

  constructor(private http: HttpClient, private dataService: DataService) { 

   

  }



   ngOnInit() {
    this.http.get(`https://api-splash.herokuapp.com/${this.profile}`)
    .subscribe( data=>{
      console.log(data)
      this.Post = data;
     
    });
   
}
}
