import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { DataService } from '../service/data.service';
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  userForm: FormGroup;
  public username:string="";
  public date = new Date()

    constructor(private router: Router, public toastController: ToastController,private zone: NgZone, public formBuilder: FormBuilder,private dataService: DataService) { 
    
      this.userForm =  this.formBuilder.group({
      post: [''],
       username:[localStorage.getItem('username')],
      time:[this.date.getMonth() +"-"+this.date.getDate()+"/"+this.date.getHours()+":"+this.date.getMinutes()]
     
    })

   }

  ngOnInit() {
       
 
  }


  postSplash(){
     if (!this.userForm.valid) {
      return false;
    } else {
      this.dataService.createPost(this.userForm.value)
        .subscribe((response) => {
          this.zone.run(() => {
            this.userForm.reset();
            this.router.navigate(['/usr/home']);
          })
        });
    }
  }
}
