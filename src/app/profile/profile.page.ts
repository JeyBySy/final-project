import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public firstName:string = ""
  public lastName:string = ""
   public username:string = ""


  Post: any = [];
  uname = localStorage.getItem('username')
  public profile = `${this.uname}`;
  constructor(private router: Router,private http: HttpClient, ) { }

  ngOnInit() {
    this.firstName = localStorage.getItem('firstName')
    this.lastName = localStorage.getItem('lastName')
    this.username = localStorage.getItem('username')

    this.http.get(`https://api-splash.herokuapp.com/splash/${this.profile}`)
    .subscribe( data=>{
      console.log(data)
      this.Post = data;
     
    });

  }

  logout(){
    localStorage.setItem('isLogin','false')
    this.router.navigateByUrl('/login')
  }

}
