import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutologinGuard implements CanLoad {
   constructor(private router:Router){}
  canLoad(){
    // console.log("hello")
    //  return true;
    const isLogin = (localStorage.getItem('isLogin') === "true" && localStorage.getItem('isLogin') != null)
    
    if(isLogin){
      this.router.navigateByUrl('/usr/home')
      return true;
    }
    else{
      return true
    }

    
  }
}
