import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(private router:Router){}
  canLoad(){
    // const isAuthenticated = !!((+localStorage.getItem('authenticated')));
    const isAuthenticated = (localStorage.getItem('isLogin')==='true')
    if(isAuthenticated){
      return true
    }else{
      const navigation = this.router.getCurrentNavigation();

      // console.log(navigation)
      let url = '/'
      if(navigation){
        url = navigation.extractedUrl.toString();
      }
      

      this.router.navigateByUrl('/')
      return false
    }
  }
}
