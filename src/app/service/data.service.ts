import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

export class Post {
  post:string;
}

@Injectable({
  providedIn: 'root'
})


export class DataService {
httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }
  

  createPost(user: Post): Observable<any> {
    return this.http.post<Post>('https://api-splash.herokuapp.com/splash/add', user, this.httpOptions)
      .pipe(
        catchError(this.handleError<Post>('Error occured'))
      );
  }

  getForum(id){
    return this.http.get('https://api-splash.herokuapp.com/forum/'+id);
  }
  //  getUser(id): Observable<Post[]> {
  //   return this.http.get<Post[]>('http://localhost:3000/splash/add' + id)
  //     .pipe(
  //       tap(_ => console.log(`User fetched: ${id}`)),
  //       catchError(this.handleError<Post[]>(`Get user id=${id}`))
  //     );
  // }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };


}
}
